
import { IsString, IsUUID, IsDate, IsEmail, IsOptional, } from 'class-validator';
import { Traveler, Gender } from './traveler.entity';
import { BaseDto } from './../base/base.dto';

export class TravelerDto extends BaseDto{
    @IsOptional()
    id: string;
  
    @IsDate()
    birthDate: Date;

    @IsString()
    firstName: string;

    @IsEmail()
    email: string;


    @IsString()
    lastName: string;

    @IsString()
    profileUri: string;

    @IsString()
    password: string


    public static async from(dto: Partial<TravelerDto>): Promise<TravelerDto>{
      const it = new TravelerDto();
      it.id = dto.id
      it.email = dto.email;
      it.firstName = dto.firstName;
      it.lastName = dto.lastName;
      it.profileUri = dto.profileUri;
      it.password = dto.password;
      it.birthDate = dto.birthDate
      return it
    }
  
    public static async fromEntity(entity: Traveler): Promise<TravelerDto>{
      return this.from({
        id: entity.id,
        birthDate: await TravelerDto.dateToCalendar(entity.birthDate),
        email: entity.email,
        firstName: entity.firstName,
        lastName: entity.lastName,
        profileUri: entity.profileUri,
      });
    }
  
    public  async toEntity(): Promise<Traveler> {
      const it = new Traveler();
      it.id = this.id;
      it.birthDate = await TravelerDto.calendarToDate(this.birthDate);
      it.email = this.email;
      it.firstName = this.firstName;
      it.lastName = this.lastName;
      it.profileUri = this.profileUri;
      return it
    }
}