import { Controller, Get, Post, Body, Query, Param, Req, UseGuards } from '@nestjs/common';
import { TravelerService } from './traveler.service';
import { Traveler } from './traveler.entity';
import { TravelerDto } from './traveler.dto';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';

@Controller('travelers')
export class TravelerController {
    constructor(private serv: TravelerService) { }
    
    @Get()
    @UseGuards(AuthGuard('firebase'))
    public async getAll(@Req() req: Request): Promise<TravelerDto[]> {
        return await this.serv.findAll()
    }
    @UseGuards(AuthGuard('firebase'))    
    @Get('/:id')
    public async get(@Req() req: Request): Promise<TravelerDto>{
        console.log(req.headers.authorization)
        return await this.serv.findTraveler(req.params.id)
    }

    @Post('/create')
    public async create(@Req()req: Request): Promise<TravelerDto>{
        return await this.serv.create(await TravelerDto.from(req.body))
    }
}
