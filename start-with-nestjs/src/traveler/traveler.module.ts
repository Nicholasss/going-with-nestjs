import { Module } from '@nestjs/common';
import { TravelerService } from './traveler.service';
import { TravelerController } from './traveler.controller';
import { TravelerRepository } from './traveler.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
imports:[TypeOrmModule.forFeature([TravelerRepository])],
providers: [TravelerService],
controllers: [TravelerController],
exports: [TypeOrmModule.forFeature([TravelerRepository]), TravelerService]
})
export class TravelerModule {}
