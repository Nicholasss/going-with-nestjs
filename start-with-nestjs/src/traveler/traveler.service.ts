import { Injectable } from '@nestjs/common';
import { Traveler } from './traveler.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { TravelerRepository } from './traveler.repository';
import { TravelerDto } from './traveler.dto';
import * as bcrypt from "bcryptjs"
import { DeleteResult } from 'typeorm';
@Injectable()
export class TravelerService {
    constructor(@InjectRepository(Traveler) private readonly repo: TravelerRepository) { }

        async findAll(): Promise<TravelerDto[]>{
            const travelers: Traveler[] = await this.repo.find()
            const travelerDtos: TravelerDto[] = []
            travelers.forEach(async (traveler: Traveler)=>{
                travelerDtos.push(await TravelerDto.fromEntity(traveler))
            })
            return travelerDtos;
        }

        async findTraveler(id: string): Promise<TravelerDto>{
            const traveler: Traveler =  await this.repo.findOne({id})
            const travelerDto = await TravelerDto.fromEntity(traveler)
            return travelerDto;
        }

        async create(traveler: TravelerDto): Promise<TravelerDto>{
            const result = await this.repo.save(await traveler.toEntity())
            return await TravelerDto.fromEntity(result);
        }

        async deleteTraveler(id: string): Promise<DeleteResult>{
            return await this.repo.delete(id)
        }
    
}
