import { EntityRepository, Repository } from "typeorm";
import { Traveler } from './traveler.entity';

@EntityRepository(Traveler)
export class TravelerRepository extends Repository<Traveler> {
// incase if there is unique
}
