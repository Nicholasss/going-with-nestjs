import { Entity,Column,BeforeInsert, PrimaryColumn } from "typeorm";
import { BaseEntity } from '../base/base.entity';
import * as crypto from 'crypto'
import * as bcrypt from "bcryptjs"
import { Constants } from "../constants/constants";
/** Gender types. */
export enum Gender {
  male = 'Male',
  female = 'Female',
}

@Entity('Traveler')
export class Traveler {
  @PrimaryColumn()
  public id : string;
  /** The traveler's email address. */
  @Column({unique: true})
  public email: string;

  /** The traveler's gender. */

  /** The traveler's last name. */
  @Column()
  public lastName: string;

  /** The traveler's first name. */
  @Column()
  public firstName: string;

  /** The travelers birthdate. */
  @Column('timestamp without time zone')
  public birthDate: Date;

  @Column()
  public profileUri: string;

  @Column({nullable: true})
  public baseCurrency: string;
  // @Column()
  // public password: string;

  // @BeforeInsert()
  // async hashPassword(){
  // this.password = await bcrypt.hash(this.password, Constants.saltRound)
  // }
}
