import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';
import { TravelerController } from './traveler/traveler.controller';
import { TravelerService } from './traveler/traveler.service';
import { TravelerModule } from './traveler/traveler.module';
import { FirebaseauthModule } from './firebaseauth/firebaseauth.module';
@Module({
  imports: [
  TypeOrmModule.forRoot(configService.getTypeOrmConfig()), TravelerModule, FirebaseauthModule],
  controllers: [AppController, TravelerController],
  providers: [AppService, TravelerService],
})
export class AppModule{}
