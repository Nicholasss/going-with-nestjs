import * as firebaseAdmin from 'firebase-admin'
import * as key from './firebaseAdmin-key.json'
export const firebaseConfig: any = {
  apiKey: "AIzaSyAYXZSjqa5rq_sOtiXB04FebpPT8AHx6Xs",
    authDomain: "nestjs-auth-476d7.firebaseapp.com",
    databaseURL: "https://nestjs-auth-476d7.firebaseio.com",
    projectId: "nestjs-auth-476d7",
    storageBucket: "nestjs-auth-476d7.appspot.com",
    messagingSenderId: "400484523018",
    appId: "1:400484523018:web:93e41dc4130090f03113f3",
    measurementId: "G-NGW171BZEL"
}



const serviceAccount: firebaseAdmin.ServiceAccount ={
  projectId: key.project_id,
  clientEmail: key.client_email,
  privateKey: key.private_key,
}

export const firebaseAdminConfig: any = {
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: "https://going-96169.firebaseio.com"
}