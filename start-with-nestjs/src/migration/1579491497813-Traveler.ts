import {MigrationInterface, QueryRunner} from "typeorm";

export class Traveler1579491497813 implements MigrationInterface {
    name = 'Traveler1579491497813'

    public async up(queryRunner: QueryRunner): Promise<any> {
       
        await queryRunner.query(`ALTER TABLE "Traveler" ADD "password" character varying NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" DROP COLUMN "password"`, undefined);
    }

}
