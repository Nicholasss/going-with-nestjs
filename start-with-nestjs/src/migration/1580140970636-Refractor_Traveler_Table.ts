import {MigrationInterface, QueryRunner} from "typeorm";

export class RefractorTravelerTable1580140970636 implements MigrationInterface {
    name = 'RefractorTravelerTable1580140970636'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" DROP CONSTRAINT "PK_556d98d6ea75d284c2c00e774e0"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" DROP COLUMN "id"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ADD "id" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ADD CONSTRAINT "PK_556d98d6ea75d284c2c00e774e0" PRIMARY KEY ("id")`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" DROP CONSTRAINT "PK_556d98d6ea75d284c2c00e774e0"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" DROP COLUMN "id"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ADD CONSTRAINT "PK_556d98d6ea75d284c2c00e774e0" PRIMARY KEY ("id")`, undefined);
    }

}
