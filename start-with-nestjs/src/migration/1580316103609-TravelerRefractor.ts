import {MigrationInterface, QueryRunner} from "typeorm";

export class TravelerRefractor1580316103609 implements MigrationInterface {
    name = 'TravelerRefractor1580316103609'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" RENAME COLUMN "profilPic" TO "profilePic"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" RENAME COLUMN "profilePic" TO "profilPic"`, undefined);
    }

}
