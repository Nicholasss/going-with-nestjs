import {MigrationInterface, QueryRunner} from "typeorm";

export class Traveler1579492224884 implements MigrationInterface {
    name = 'Traveler1579492224884'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" ADD CONSTRAINT "UQ_4a007ec864056852ba7177bbce6" UNIQUE ("email")`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ADD CONSTRAINT "UQ_f8576281dd8ddb762c11f491340" UNIQUE ("username")`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" DROP CONSTRAINT "UQ_f8576281dd8ddb762c11f491340"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" DROP CONSTRAINT "UQ_4a007ec864056852ba7177bbce6"`, undefined);
    }

}
