import {MigrationInterface, QueryRunner} from "typeorm";

export class TravelerRefractor1580313044935 implements MigrationInterface {
    name = 'TravelerRefractor1580313044935'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" RENAME COLUMN "gender" TO "baseCurrency"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ALTER COLUMN "baseCurrency" DROP NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" ALTER COLUMN "baseCurrency" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" RENAME COLUMN "baseCurrency" TO "gender"`, undefined);
    }

}
