import {MigrationInterface, QueryRunner} from "typeorm";

export class DropBaseEntity1579260525892 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("DROP TABLE base_entity")
    }


    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
