import {MigrationInterface, QueryRunner} from "typeorm";

export class TravelerTable1579260331692 implements MigrationInterface {
    name = 'TravelerTable1579260331692'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "Traveler" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "email" character varying NOT NULL, "username" character varying NOT NULL, "gender" character varying NOT NULL, "lastName" character varying NOT NULL, "firstName" character varying NOT NULL, "birthDate" TIMESTAMP NOT NULL, "profilPic" character varying NOT NULL, CONSTRAINT "PK_556d98d6ea75d284c2c00e774e0" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "Traveler"`, undefined);
    }

}
