import {MigrationInterface, QueryRunner} from "typeorm";

export class Traveler1579492053139 implements MigrationInterface {
    name = 'Traveler1579492053139'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" ADD "password" character varying NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" DROP COLUMN "password"`, undefined);
    }

}
