import {MigrationInterface, QueryRunner} from "typeorm";

export class TravelerRemoveUsernameColumn1580312553133 implements MigrationInterface {
    name = 'TravelerRemoveUsernameColumn1580312553133'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" DROP CONSTRAINT "UQ_f8576281dd8ddb762c11f491340"`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" DROP COLUMN "username"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" ADD "username" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "Traveler" ADD CONSTRAINT "UQ_f8576281dd8ddb762c11f491340" UNIQUE ("username")`, undefined);
    }

}
