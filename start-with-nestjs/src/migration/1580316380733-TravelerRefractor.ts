import {MigrationInterface, QueryRunner} from "typeorm";

export class TravelerRefractor1580316380733 implements MigrationInterface {
    name = 'TravelerRefractor1580316380733'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" RENAME COLUMN "profilePic" TO "profileUri"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Traveler" RENAME COLUMN "profileUri" TO "profilePic"`, undefined);
    }

}
