import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TravelerModule } from '../traveler/traveler.module';
import { FirebaseStrategy } from './firebase.strategy';
@Module({
  imports: [
  PassportModule,
  ],
  controllers: [],
  providers: [FirebaseStrategy]
})
export class FirebaseauthModule {}
