import { Injectable, HttpStatus, Req, Res } from "@nestjs/common";
import { Strategy } from "passport-local";
import { PassportStrategy } from '@nestjs/passport';
import * as firebaseAdmin from 'firebase-admin'
import { Request } from 'express';
@Injectable()
export class FirebaseStrategy extends PassportStrategy(Strategy,'firebase'){
    constructor(){
        super()
    }
    async authenticate(@Req() req: Request):Promise<void>{
        const authorization = req.headers.authorization;
        console.log(authorization)
        if(authorization===null || authorization === undefined){
            console.log("Firebase token auth failed")
            this.fail(HttpStatus.UNAUTHORIZED)
            return;
        }
        const token = authorization.slice(7)
        const credential = await firebaseAdmin.auth().verifyIdToken(token).catch(err =>{
            console.log(err)
            this.fail(HttpStatus.UNAUTHORIZED)
            return;
        })
        console.log("Firebase token auth success")
        this.success({})
    }
}