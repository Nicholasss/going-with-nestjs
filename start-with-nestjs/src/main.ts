import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as firebaseAdmin from 'firebase-admin'
import { firebaseConfig, firebaseAdminConfig } from './config/config.firebase';

async function bootstrap() {
  firebaseAdmin.initializeApp(firebaseAdminConfig)
  const app = await NestFactory.create(AppModule);
  await app.listen(5000);
}
bootstrap();
