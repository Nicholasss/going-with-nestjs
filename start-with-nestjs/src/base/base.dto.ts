
export class BaseDto implements Readonly<BaseDto>{
    
    public static async calendarToDate(calendar: any):Promise<Date>{
        const date = new Date()
        date.setFullYear(calendar.year)
        date.setMonth(calendar.month-1)
        date.setHours(calendar.hourOfDay)
        date.setMinutes(0)
        date.setSeconds(0)
        return await date
      }
  
      public static async dateToCalendar(date:Date):Promise<any>{
        // if(date.constructor.name!=="Date"){
        //   return date;
        // }
        const calendar: any = {}
        calendar.year = date.getFullYear()
        calendar.dayOfMonth = date.getMonth()
        calendar.hourOfDay = date.getHours()
        calendar.minute = 0
        calendar.second= 0
        return await calendar
      }
}